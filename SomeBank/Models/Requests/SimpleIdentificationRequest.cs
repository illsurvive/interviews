﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SevenSeconds.Integration.Finance.TransCapitalBank.Models.Requests
{
    //[XmlRoot("SimpleIdentificationRequest")]
    public class SimplifiedPersonIdentificationRequest
    {
        /// <summary>
        /// Внешний уникальный идентификатор заявки
        /// </summary>
        public string ExtId { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime BirthDay { get; set; }
        /// <summary>
        /// ИНН
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Inn { get; set; }
        /// <summary>
        /// СНИЛС
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Snils { get; set; }
        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string Series { get; set; }
        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        public DateTime IssueDate { get; set; }
        /// <summary>
        /// Код подразделения, формат NNN-NNN, где N- число
        /// </summary>
        public string IssuerCode { get; set; }
        /// <summary>
        /// Кем выдан
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// Номер телефона, в формате 10 цифр
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}
