﻿using SevenSeconds.Integration.Finance.TransCapitalBank.Models;
using SevenSeconds.Integration.Finance.TransCapitalBank.Models.Requests;
using SevenSeconds.Integration.Finance.TransCapitalBank.Models.Responses;
using SevenSeconds.Models.Core;
using SevenSeconds.Models.Sys;
using SevenSeconds.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SevenSeconds.Models.Map;
using SevenSeconds.Util.Exec;

namespace SevenSeconds.Integration.Finance.TransCapitalBank
{
    public class TransCapitalBankClient
    {
        private readonly ApiJsonClient _client;
        private readonly FinanceLogger _logger;
        private readonly string _login = "sensetive data";
        private readonly string _sign = "sensetive data";
        private readonly string _requiredConfirmationCode = "RequiredConfirmationCode";

        public TransCapitalBankClient()
        {
            var url = "ыутыу;
            _logger = new FinanceLogger();
            _client = new ApiJsonClient(url);
        }

        /// <summary>
        ///     Первичный запрос на идентификацию
        ///     Пользователь получит SMS
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public bool IdentificationRequest(Guid applicationId)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var app = context.A_Applications.Single(a => a.Application_ID == applicationId);

                app.IdentificationRequestId = Guid.NewGuid();
                context.SaveChanges();

                var result = SimplifiedPersonIdentification(app);

                if (result.Code != _requiredConfirmationCode)
                {
                    return false;
                }

                app.IdentificationOperationId = result.Properties.OperationId;
                context.SaveChanges();

                return true;
            }
        }

        /// <summary>
        ///     Подтверждение идентификации SMS кодом и получение ее результатов
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="smsCode"></param>
        /// <returns></returns>
        public IdentificationResult IdentificationConfirm(Guid applicationId, string smsCode)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var app = context.A_Applications.Single(a => a.Application_ID == applicationId);

                var chainResult = Exec.StartChain(node =>
                {
                    var result = SimplifiedPersonIdentification(app, smsCode, app.IdentificationOperationId);

                    if (result.Code == IdentificationResult.ConfirmationCodeExpired.ToDescription())
                    {
                        node.EndAs(ExecState.Error, IdentificationResult.ConfirmationCodeExpired.ToDescription());
                        return;
                    }

                    if (result.Code == IdentificationResult.ConfirmationCodeInvalid.ToDescription())
                    {
                        node.EndAs(ExecState.Error, IdentificationResult.ConfirmationCodeInvalid.ToDescription());
                        return;
                    }

                    if (!string.IsNullOrWhiteSpace(result.ExceptionType))
                    {
                        node.EndAs(ExecState.Error, IdentificationResult.UnexpectedError.ToDescription());
                        return;
                    }
                })
                .ThenPeriodic(1000, 1000, 3 * 60 * 1000, node =>
                {
                    var result = SimplifiedPersonIdentificationResult(app);

                    if (!string.IsNullOrWhiteSpace(result.ExceptionType))
                    {
                        node.EndAs(ExecState.Error, IdentificationResult.UnexpectedError.ToDescription());
                        return;
                    }

                    if (result.Passport.Status == "Processing" || result.Inn.Status == "Processing" || result.Snils.Status == "Processing")
                    {
                        return;
                    }

                    if (result.Passport.Status == "Valid" && (result.Inn.Status == "Valid" || result.Snils.Status == "Valid"))
                    {
                        node.EndAs(ExecState.Success, IdentificationResult.IdentificationSucceeded.ToDescription());
                        return;
                    }
                    
                    node.EndAs(ExecState.Fail, IdentificationResult.IdentificationFailed.ToDescription());
                    return;
                    
                })
                .Launch();

                app.IsIdentified = chainResult.Message == IdentificationResult.IdentificationSucceeded.ToDescription();
                context.SaveChanges();

                // Я б сделать это смог и на свитч-кейс
                // Но здесь за литералы казнить могут :'(
                if (chainResult.Message == IdentificationResult.IdentificationSucceeded.ToDescription())
                {
                    return IdentificationResult.IdentificationSucceeded;
                }
                if (chainResult.Message == IdentificationResult.IdentificationFailed.ToDescription())
                {
                    return IdentificationResult.IdentificationFailed;
                }
                if (chainResult.Message == IdentificationResult.ConfirmationCodeInvalid.ToDescription())
                {
                    return IdentificationResult.ConfirmationCodeInvalid;
                }
                if (chainResult.Message == IdentificationResult.ConfirmationCodeExpired.ToDescription())
                {
                    return IdentificationResult.ConfirmationCodeExpired;
                }
                if (chainResult.Message == IdentificationResult.UnexpectedError.ToDescription())
                {
                    return IdentificationResult.UnexpectedError;
                }

                return IdentificationResult.Unknown;
            }
        }

        /// <summary>
        /// Запрос на идентификацию
        /// </summary>
        /// <param name="application">Заявка</param>
        /// <returns></returns>
        private SimpleIdentificationResponse SimplifiedPersonIdentification(A_Applications application, string smsCode = null, string operationId = null)
        {
            var requestTime = DateTime.Now;

            var person = application.P_Persons;
            var document = person.P_Documents.OrderByDescending(d => d.CreateDate).First();

            var request = new SimplifiedPersonIdentificationRequest {
                ExtId = application.IdentificationRequestId.Value.ToString(),
                FirstName = person.FirstName,
                LastName = person.LastName,
                Patronymic = person.MiddleName,
                BirthDay = person.BirthDate.Value,
                Series = document.Series,
                Number = document.Number,
                IssueDate = document.IssueDate ?? DateTime.MinValue ,
                IssuerCode = document.DivisionCode,
                Issuer = document.IssuedBy,
                PhoneNumber = person.Phone.ToPhone10DigitFormat()
            };

            if (!String.IsNullOrWhiteSpace(person.Inn))
            {
                request.Inn = person.Inn;
            }
            else if (!String.IsNullOrWhiteSpace(person.Snils))
            {
                request.Snils = ToSnils(person.Snils);
            }


            var hash = Encode(request.ToJson<SimplifiedPersonIdentificationRequest>().ToString(), Encoding.ASCII.GetBytes(_sign));

            var headers = new List<CustomHttpHeader>()
            {
                new CustomHttpHeader("SomeAgent-Header-Login", _login),
                new CustomHttpHeader("SomeAgent-Header-Sign", hash)
            };

            if (!string.IsNullOrWhiteSpace(smsCode) && !string.IsNullOrWhiteSpace(operationId))
            {
                headers.Add(new CustomHttpHeader("SomeAgent-Header-ConfirmationCode", $"{smsCode};{operationId}"));
            }

            var response = _client.Post<SimplifiedPersonIdentificationRequest, SimpleIdentificationResponse>(request,
                "/api/government/identification/simplifiedpersonidentification",
                headers.ToArray());

            _logger.Log<TransCapitalBank_Log>(application.Application_ID, "SimplifiedPersonIdentification",
                request.ToJson<SimplifiedPersonIdentificationRequest>(), response.ToJson<SimpleIdentificationResponse>(), requestTime);

            return response;
        }

        /// <summary>
        /// Запрос на получение результатов идентификации(проверяем паспорт и СНИЛС)
        /// </summary>
        /// <param name="application">Заявка</param>
        /// <returns>SimpleIdentificationResponse Snils.Status - статус СНИЛС, Passport.Status статус паспорта</returns>
        /// Возможные значения проверки статуса СНИЛС и паспорта: Error, NotProcessed, Processing, Valid, NotValid
        private SimplifiedPersonIdentificationResultResponse SimplifiedPersonIdentificationResult(A_Applications application)
        {
            var requestTime = DateTime.Now;

            var request = new SimplifiedPersonIdentificationResultRequest {
                ExtId = application.IdentificationRequestId.Value.ToString()
            };

            var hash = Encode(request.ToJson<SimplifiedPersonIdentificationResultRequest>().ToString(), Encoding.ASCII.GetBytes(_sign));


            var response = _client.Post<SimplifiedPersonIdentificationResultRequest, SimplifiedPersonIdentificationResultResponse>(request,
                "/api/government/identification/simplifiedpersonidentificationresult",
                new CustomHttpHeader[] {
                                            new CustomHttpHeader("SomeAgent-Header-Login", _login),
                                            new CustomHttpHeader("SomeAgent-Header-Sign", hash)
                });

            _logger.Log<TransCapitalBank_Log>(application.Application_ID, "SimplifiedPersonIdentificationResult",
                request.ToJson<SimplifiedPersonIdentificationResultRequest>(), 
                response.ToJson<SimplifiedPersonIdentificationResultResponse>(), requestTime);

            return response;
        }

        private string ToSnils(string snils)
        {
            return $"{snils.Substring(0, 11)} {snils.Substring(12, 2)}";
        }

        private string Encode(string input, byte[] key)
        {
            HMACSHA1 hmacsha1 = new HMACSHA1(key);
            byte[] byteArray = Encoding.UTF8.GetBytes(input);
            MemoryStream stream = new MemoryStream(byteArray);

            return Convert.ToBase64String(hmacsha1.ComputeHash(stream));
        }
    }
}