﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SevenSeconds.Integration.Finance.TransCapitalBank.Models.Requests
{
    [XmlRoot("GetIdentificationState")]
    public class GetIdentificationStateRequest
    {
        public string OrderId { get; set; }
    }
}
