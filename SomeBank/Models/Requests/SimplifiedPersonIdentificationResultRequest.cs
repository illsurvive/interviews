﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SevenSeconds.Integration.Finance.TransCapitalBank.Models.Requests
{
    public class SimplifiedPersonIdentificationResultRequest
    {
        /// <summary>
        /// Внешний уникальный идентификатор заявки
        /// </summary>
        public string ExtId { get; set; }
    }
}
