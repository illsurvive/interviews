﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SevenSeconds.Integration.Finance.TransCapitalBank.Models
{
    public class Document
    {
        public string SerialNo { get; set; }
        public string Number { get; set; }
        public DateTime IssuingDate { get; set; }
        public string Issuer { get; set; }
        public string IssuerCode { get; set; }
    }
}
