﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SevenSeconds.Integration.Finance.TransCapitalBank
{
    public enum IdentificationResult
    {
        [Description("IdentificationSucceeded")]
        IdentificationSucceeded,

        [Description("IdentificationFailed")]
        IdentificationFailed,

        [Description("ConfirmationCodeInvalid")]
        ConfirmationCodeInvalid,

        [Description("ConfirmationCodeExpired")]
        ConfirmationCodeExpired,

        [Description("UnexpectedError")]
        UnexpectedError,

        [Description("Unknown")]
        Unknown
    }
}
