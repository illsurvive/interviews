﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SevenSeconds.Integration.Finance.TransCapitalBank.Models.Responses
{
    [XmlRoot("GetIdentificationState")]
    public class GetIdentificationStateResponse
    {
        public string orderID { get; set; }
        public string orderNumber { get; set; }
        public int confirmed { get; set; }
        public int passportValid { get; set; }
        public int snilsValid { get; set; }
        public int omsValid { get; set; }
        public int innValid { get; set; }
        //public additionalInfo additionalInfo { get; set; }
        public orderInfo orderInfo { get; set; }
        public errorInfo errorInfo { get; set; }
    }

    //public class additionalInfo
    //{
    //    public string CONFIRMED { get; set; }
    //    public string PERSONVALID { get; set; }
    //    public string PASSPORTVALID { get; set; }
    //    public string SNILSVALID { get; set; }
    //    public string OMSVALID { get; set; }
    //    public string INNVALID { get; set; }
    //}

    public class orderInfo
    {
        public string orderID { get; set; }
        public string orderNumber { get; set; }
        public string state { get; set; }
        public string stateDescription { get; set; }
        public int amount { get; set; }
    }
}
