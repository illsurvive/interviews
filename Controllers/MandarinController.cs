﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using SevenSeconds;
using SevenSeconds.Integration.Mandarin;
using SevenSeconds.Models.Core;
using SevenSeconds.Models.Core.StateMachine;
using SevenSeconds.Models.Js;
using SevenSeconds.Models.Js.Application;
using SevenSeconds.Models.Js.Integration;
using SevenSeconds.Models.Map;
using SevenSeconds.Util;
using System.Data;
using System.Data.Entity;

namespace CoreAPI.Controllers.api
{
    public class MandarinController : ApiController
    {
        /// <summary>
        ///     Callback-уведомления, присылаемые при проведении транзакций.
        /// </summary>
        /// <returns></returns>
        /// <remarks>http://docs.mandarinbank.com/?csharp#card_binding-callback</remarks>
        [AllowAnonymous]
        [HttpPost]
        public ApiResponse TransactionCallback(MandarinTransactionCallbackModel model)
        {
            var mandarinCallback = model.ToMandarinCallback();
            using (var context = new SevenSecondsCoreEntities())
            {
                context.A_MandarinCallback.Add(mandarinCallback);
                context.SaveChanges();
                context.Entry(mandarinCallback).Reload();
                //Сбрасываем поток на начало, т.к. его уже прочитала среда для формирования модели.
                Request.Content.ReadAsStreamAsync().Result.Seek(0, SeekOrigin.Begin);
                var formData = Request.Content.ReadAsStringAsync().Result;
                if (!MandarinHelper.VerifyModelSign(formData, model.sign))
                {
                    return new ApiResponse("sign", "Запрос неверно подписан.");
                }

                //На продовой конфигурации запрещаем принимать платежи из песочницы
#if PRODUCTION
                if (mandarinCallback.Sandbox == true)
                {
                    return new ApiResponse("sandbox", "Тестовые карты к оплате не принимаются.");
                }
#endif

                DbContextTransaction transaction = null;
                ApiResponse response = null;
                try
                {
                    transaction = context.Database.BeginTransaction(IsolationLevel.Serializable);
                    var app = context.A_Applications
                        .Include("A_Statuses")
                        .Single(a => a.Application_ID == mandarinCallback.ApplicationId);

                    var curTransaction = app.A_InitFeePaymentRequests.SingleOrDefault(x => x.OperationId == mandarinCallback.TransactionId);

                    if (mandarinCallback.Status != "success")
                    {
                        //Если платеж был сформирован по ссылке
                        if (curTransaction != null)
                        {
                            curTransaction.IsPaid = false;
                            context.SaveChanges();
                        }
                        if (!app.HasLastStatus(ApplicationStatus.CardHoldFail))
                        {
                            if (app.HasLastStatus(ApplicationStatus.ApprFO))
                            {
                                app.ChangeStatus(ApplicationStatus.PaymentInitiated);
                                app.LastStatus.StatusDate = app.LastStatus.StatusDate.AddSeconds(-1);
                            }

                            app.ChangeStatus(ApplicationStatus.CardHoldFail);
                            context.SaveChanges();
                        }
                        return new ApiResponse("status", "Некорректный статус: " + mandarinCallback.Status);
                    }

                    switch (mandarinCallback.Action)
                    {
                        case "preauth":
                            //if (mandarinCallback.Price != mandarinCallback.A_Applications.MandarinPaymentSum)
                            //{
                            //    if (!app.HasLastStatus(ApplicationStatus.CardHoldFail))
                            //    {
                            //        app.ChangeStatus(ApplicationStatus.CardHoldFail);
                            //        context.SaveChanges();
                            //    }
                            //    return new ApiResponse("price", "Некорректная сумма платежа");
                            //}

                            //if (mandarinCallback.Sandbox != true)
                            //{
                            //    var atolResult = new ApiJsonClient($"{SevenSeconds.Helpers.ConfigurationHelper.CoreApiUrl}").Post<AppRequest, ApiResponse>(new AppRequest { application_id = app.Application_ID.ToString() }, "/api/Fiscal/GetBill").result;

                            //    if (!atolResult)
                            //    {
                            //        MandarinHelper.Reversal(app.Application_ID);
                            //        app.AddNewStatus(ApplicationStatus.PaymentError);
                            //        context.SaveChanges();
                            //        return new ApiResponse("fns", "Ошибка отправки данных в ФНС");
                            //    }
                            //}

                            if (app.A_InitFeePaymentRequests.Any())
                            {
                                //Если платеж был сформирован по ссылке
                                if (curTransaction != null)
                                {
                                    curTransaction.IsPaid = true;
                                    //Если больше нет неоплаченных ссылок - ставим статус
                                    if (!app.A_InitFeePaymentRequests.Where(op => op.IsPaid != true).Any())
                                    {
                                        if (app.HasLastStatus(ApplicationStatus.ApprFO))
                                        {
                                            app.ChangeStatus(ApplicationStatus.PaymentInitiated);
                                            app.LastStatus.StatusDate = app.LastStatus.StatusDate.AddSeconds(-1);
                                        }

                                        app.ChangeStatus(ApplicationStatus.CardHoldSuccess);
                                    }
                                }
                            }
                            else if ((app.MandarinInitialPayment ?? 0m) > 0)
                            {
                                //Платеж сформирован через виджет
                                if (app.HasLastStatus(ApplicationStatus.ApprFO))
                                {
                                    app.ChangeStatus(ApplicationStatus.PaymentInitiated);
                                    app.LastStatus.StatusDate = app.LastStatus.StatusDate.AddSeconds(-1);
                                }

                                app.ChangeStatus(ApplicationStatus.CardHoldSuccess);
                            }

                            app.CommissionAmount = mandarinCallback.Price;
                            context.SaveChanges();
                            response = ApiResponse.Success;
                            break;
                        case "pay":
                        case "reversal":
                            response = ApiResponse.Success;
                            break;
                        default:
                            response = new ApiResponse("action", "Некорректное действие: " + mandarinCallback.Action);
                            break;
                    }
                }
                catch
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                        transaction = null;
                    }

                    throw;
                }
                finally
                {
                    if (transaction != null)
                        transaction.Commit();
                }
                
                return response;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ApiScalarResponse CreateTransactionAndGetFormParameters(Guid applicationId)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var app = context.A_Applications.Single(x => x.Application_ID == applicationId);
                var mandarin = new MandarinInfoModel
                {
                    Price = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", app.MandarinPaymentSum),
                    DeliveryPrice = app.MandarinDeliveryPrice != null ? string.Format(CultureInfo.InvariantCulture, "{0:0.00}", app.MandarinDeliveryPrice) : null,
                    InitialPayment = app.MandarinInitialPayment != null ? string.Format(CultureInfo.InvariantCulture, "{0:0.00}", app.MandarinInitialPayment) : null,
                    SevenSecondsComission = app.MandarinComissiom != null ? string.Format(CultureInfo.InvariantCulture, "{0:0.00}", app.MandarinComissiom) : null,
                    OperationId = MandarinHelper.Preauth(app.Application_ID)
                };
                return new ApiScalarResponse(mandarin);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ApiScalarResponse CreateTransactionAndGetFormParameters(MandarinPreauthModel request)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var app = context.A_Applications.Single(x => x.Application_ID == request.ApplicationId);

                var operationId = MandarinHelper.Preauth(app, request.Amount, request.ActualTime);

                return new ApiScalarResponse(operationId);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ApiResponse ConfirmTransaction(Guid applicationId)
        {
            var result = MandarinHelper.ConfirmAuth(applicationId);
            if (string.IsNullOrEmpty(result))
            {
                return new ApiResponse("", "Ошибка подтверждения платежа");
            }
            return ApiResponse.Success;
        }

        /// <summary>
        /// Отменяет блокировку средств в Мандарине
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse ReverseTransaction(Guid applicationId)
        {
            using (var context = SsData.GetContext())
            {
                var transaction = context.A_MandarinCallback.Where(c => c.ApplicationId == applicationId &&
                                                                  c.Action == "preauth" &&
                                                                  c.Status == "success").OrderByDescending(c => c.CreateDate).ToList().LastOrDefault();

                if (transaction == null)
                {
                    return new ApiResponse("", "Не найдена транзакция блокировки средств");
                }
            }

            var result = MandarinHelper.Reversal(applicationId);
            if (string.IsNullOrEmpty(result))
            {
                return new ApiResponse("", "Ошибка отмены блокировки средств");
            }
            return ApiResponse.Success;
        }

        /// <summary>
        /// Возврат средств, полный или частичный
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse RefundTransaction(Guid applicationId)
        {
            decimal price;

            using (var context = SsData.GetContext())
            {
                var transaction = context.A_MandarinCallback.Where(c => c.ApplicationId == applicationId &&
                                                                  c.Action == "pay" &&
                                                                  c.Status == "success").OrderByDescending(c => c.CreateDate).ToList().LastOrDefault();

                if (transaction == null)
                {
                    return new ApiResponse("", "Не найдена транзакция платежа, которую возвращать");
                }

                price = transaction.Price;
            }

            var result = MandarinHelper.Refund(applicationId, price);
            if (string.IsNullOrEmpty(result))
            {
                return new ApiResponse("", "Ошибка возврата платежа");
            }
            return ApiResponse.Success;
        }
    }
}