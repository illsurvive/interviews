﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SevenSeconds.Integration.Finance.TransCapitalBank.Models.Responses
{
    public class IdentificationResponse
    {
        public string orderId { get; set; }
        public int orderNumber { get; set; }
        public errorInfo errorInfo { get; set; }
    }

    public class errorInfo
    {
        public int errorCode { get; set; }
        public int errorDescription { get; set; }
        public string errorMessage { get; set; }
    }
}
