﻿using System;
using SevenSeconds.Models.Core;
using SevenSeconds.Models.Js;
using System.Linq;
using System.Web.Http;
using SevenSeconds;
using SevenSeconds.Models.Js.Merchant;
using SevenSeconds.Util;
using SevenSeconds.Handle.DocumentSign;
using System.Globalization;
using System.Collections.Generic;
using SevenSeconds.Models.ARM;
using SevenSeconds.Handle.MerchantAffiliate;
using SevenSeconds.Models.Enums;
using SevenSeconds.Models.Core.StateMachine;
using SevenSeconds.Helpers;
using SevenSeconds.Handle.HLProduct;
using SevenSeconds.Models.Map;

namespace CoreAPI.Controllers.api
{
    public class MerchController : ApiController
    {
        public object HLPoducts { get; private set; }

        [AllowAnonymous]
        [HttpPost]
        public ApiScalarResponse GetCartItems(MerchantAppRequest model)
        {
            using (var context = SsData.GetContext())
            {
                var res = context.A_Orders.FirstOrDefault(a => a.Application_ID == model.Application_ID)?.A_CartItems.Select(x => new
                                                            {
                                                                articul = x.M_Goods.Articul,
                                                                category = x.M_Goods.Category1,
                                                                model = x.M_Goods.Model,
                                                                quantity = x.Quantity,
                                                                price = x.Price.ToString("C", new CultureInfo("RU"))
                                                            }).ToList(); 
                
                return new ApiScalarResponse(res);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ApiResponse Edit(Merchant model)
        {
            if (!DateTime.TryParseExact(model.StartDate, "dd.MM.yyyy", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out DateTime startDate))
                throw new ApplicationException($"Некорректный формат даты {model.StartDate}.");

            using (var ctx = SsData.GetContext())
            {
                using (var transaction = ctx.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    var merch = ctx.M_Merchants.Include("M_MerchantDirectOrders").First(x => x.Merchant_ID == model.Merchant_ID);

                    merch.Phone = model.Phone;
                    merch.Email = model.Email;
                    merch.ApiKey = model.ApiKey;
                    merch.StartDate = startDate;
                    merch.Site = model.Site;
                    merch.MinOrderAmount = model.MinOrderAmount.ToDecimalExt();
                    merch.MaxOrderAmount = model.MaxOrderAmount.ToDecimalExt();
                    merch.MaxOrderItemsCount = model.MaxOrderItemsCount;
                    merch.IsActive = model.IsActive;
                    merch.CanOrderInCabinet = model.CanOrderInCabinet;
                    merch.CustomerPayComission = model.CustomerPayComission?.ToDecimalExt();
                    merch.SourceEmail = model.SourceEmail;
                    merch.DestinationEmail = model.DestinationEmail;
                    merch.IncludeDeliveryPriceInLoan = model.IncludeDeliveryPriceInLoan;
                    merch.ManagerName = model.ManagerName;
                    merch.ManagerName2 = model.ManagerName2;
                    merch.ManagerPhone = model.ManagerPhone;
                    merch.CommissionEnabled = model.CommissionEnabled;
                    merch.ScoringMode = model.ScoringMode;
                    //merch.ShipmentType = model.ShipmentType;
                    merch.AlfaBankPointId = model.AlfaBankPointId;
                    merch.AlfaAgentCode = model.AlfaAgentCode;
                    merch.ExpressBankPointId = model.ExpressBankPointId;
                    merch.TinkoffPointId = model.TinkoffPointId;
                    merch.TinkoffMerchPointId = model.TinkoffMerchPointId;
                    merch.TinkoffPartnerId = model.TinkoffPartnerId;
                    merch.CreditEuropaPointId = model.CreditEuropaPointId;
                    merch.RsbPointId = model.RsbPointId;
                    merch.EnableOnlineReceipt = model.EnableOnlineReceipt;
                    merch.InsalesDiscount = model.InsalesDiscount.ToDecimalExt();
                    merch.InsalesKey = model.InsalesKey;
                    merch.IntegrationInsalesKey = model.IntegrationInsalesKey;
                    merch.DefaultLoanTerm = model.DefaultLoanTerm;
                    merch.DefaultInitialPayment = model.DefaultInitialPayment.ToDecimalExt();
                    merch.AlfaBankPointIsPersonal = model.AlfaBankPointIsPersonal;
                    merch.ExpressBankPointIsPersonal = model.ExpressBankPointIsPersonal;
                    merch.CreditEuropaPointIsPersonal = model.CreditEuropaPointIsPersonal;
                    merch.TinkoffPointIsPersonal = model.TinkoffPointIsPersonal;
                    merch.IsRsbPointPersonal = model.IsRsbPointPersonal;
                    merch.ShowInitialPaymentSetting = model.ShowInitialPaymentSetting;
                    merch.EnableEmailSend = model.EnableEmailSend;
                    merch.ShowTermSetting = model.ShowTermSetting;
                    merch.TermSettingType = model.TermSettingType;
                    merch.TermSettingSteps = model.TermSettingSteps;
                    merch.SendAppForPeriodIndicatedOnCalculator = model.SendAppForPeriodIndicatedOnCalculator;
                    merch.CallbacksEnabled = model.CallbacksEnabled;
                    merch.StatusCallbackUrl = model.StatusCallbackUrl;
                    merch.CallBackContainsPassportData = model.CallBackContainsPassportData;
                    merch.InitFeeCallBackEnabled = model.InitFeeCallBackEnabled;
                    merch.InitFeeCallBackUrl = model.InitFeeCallBackUrl;
                    merch.PrescoreCallbacksEnabled = model.PrescoreCallbacksEnabled;
                    merch.PrescoreStatusCallbackUrl = model.PrescoreStatusCallbackUrl;
                    merch.CopySettingsToChildMerchants = model.CopySettingsToChildMerchants;
                    merch.ActualAddress = model.ActualAddress;
                    merch.GoodsAvailabilityTimeout = model.GoodsAvailabilityTimeout;
                    merch.NeedGoodsAvailableComfirm = model.NeedGoodsAvailableComfirm;
                    merch.HideEmptyApplications = model.HideEmptyApplications;
                    merch.BonusEnabled = model.BonusEnabled;
                    merch.CallForAbandonedApp = model.CallForAbandonedApp;
                    merch.CallForCallBackRequest = model.CallForCallBackRequest;
                    merch.CallForFillingForm = model.CallForFillingForm;
                    merch.PochtaBankPointId = model.PochtaBankPointId;
                    merch.PochtaBankTOCode = model.PochtaBankTOCode;
                    merch.PochtaPointIsPersonal = model.PochtaPointIsPersonal;
                    merch.OtpTOCode = model.OtpTOCode;
                    merch.OtpPointIsPersonal = model.OtpPointIsPersonal;
                    merch.SequentialOfferRequestsEnabled = model.SequentialOfferRequestsEnabled;
                    merch.MerchSupportPhone = model.MerchSupportPhone;
                    merch.EnableMerchSupportPhone = model.EnableMerchSupportPhone;
                    merch.ShowMarketingHeader = model.ShowMarketing;
                    merch.MarketingHeaderHtml = model.MarketingHeaderHtml;
                    merch.ShowLoanPreconditions = model.ShowLoanPreconditions;
                    merch.OtpIsOnline = model.OtpIsOnline;
                    merch.СommercialTerms = model.СommercialTerms;
                    merch.MaxDiscountFeeIsRequired = model.MaxDiscountFeeIsRequired;

                    merch.HLProduct = model.HLProduct;
                    merch.InsuranceEnabled = model.InsuranceEnabled;
                    merch.InsuranceOTPEnabled = model.InsuranceOTPEnabled;
                    merch.InsuranceExpressBankEnabled = model.InsuranceExpressBankEnabled;
                    merch.InsuranceTinkoffEnabled = model.InsuranceTinkoffEnabled;
                    merch.InsuranceAlfaBankEnabled = model.InsuranceAlfaBankEnabled;
                    merch.TinkoffSigningByHL = model.TinkoffSigningByHL;
                    if(merch.HLProduct == HLProducts.GuaranteedPayment.ToDescription())
                    {
                        merch.GPPercent = model.GPPercent.ToDecimalExt(); 
                        merch.GPCreditRateEdge = model.GPCreditRateEdge.ToDecimalExt(); 
                    }

                    merch.DeferredPaymentShowMode = model.DeferredPaymentShowMode;
                    merch.DeferredPaymentShowModeAgent = model.DeferredPaymentShowModeAgent;
                    merch.AgentDiscountEnabled = model.AgentDiscountEnabled;
                    if (merch.AgentDiscountEnabled ?? false)
                    {
                        merch.MaxAgentDiscount = model.MaxAgentDiscount.ToDecimalExt(); 
                        merch.DefaultAgentDiscount = model.DefaultAgentDiscount.ToDecimalExt(); 
                    }

                    merch.PercentageCalculatingPrinciple = model.PercentageCalculatingPrinciple == "" ? null : model.PercentageCalculatingPrinciple;
                    merch.SendOffersToDeferredPayments = model.SendOffersToDeferredPayments;
                    merch.CalcDiscount7Seconds = model.CalcDiscount7Seconds;

                    if(merch.CalcDiscount7Seconds ?? false)
                    {
                        merch.CalcDiscount7SecondsMode = model.CalcDiscount7SecondsMode;
                        if (merch.CalcDiscount7SecondsMode == (int)CalcDiscount7SecondsModes.ByMaxDiscount)
                        {
                            merch.MerchantMaxDiscount = model.MerchantMaxDiscount.ToDecimalExt(); 
                            merch.MerchantDiscountMode = model.MerchantDiscountMode;
                        }
                    }

                    merch.DeferredPaymentMoneyDfference = model.DeferredPaymentMoneyDfference.ToDecimalExt();

                    merch.RewardCalcByBaseRate = model.RewardCalcByBaseRate;
                    merch.RewardBeforeTransferToMerch = model.RewardBeforeTransferToMerch;
                    if (merch.RewardCalcByBaseRate ?? false)
                    {
                        merch.RewardBaseRate = model.RewardBaseRate.ToDecimalExt(); 
                    }

                    var settings = merch.MerchantSettings;
                    if (model.Discounts != null)
                        settings.Discounts = model.Discounts;

                    if (model.DiscountChannels != null)
                        settings.DiscountChannels = model.DiscountChannels;

                    if (model.Notifications != null)
                        settings.Notifications = model.Notifications;

                    merch.MerchantSettings = settings;
                    


                    var errors = ProcessMerchantCreditProducts(merch.M_MerchantDisabledCreditProducts, model.CreditProducts, model.Merchant_ID, ctx);
                    if (errors != null && errors.Count > 0)
                        return new ApiResponse(errors);

                    ProcessMerchantStatusCallbackSettings(merch, model.CallbackSettings, ctx);

                    merch.IsDirectOrderEnabled = model.DirectOrderEnabled;
                    if (model.DirectOrderEnabled)
                    {
                        merch.M_MerchantDirectOrders = model.DirectOrder.ToMerchantDirectOrder();
                        merch.M_MerchantDirectOrders.Merchant_ID = merch.Merchant_ID;
                        merch.M_MerchantDirectOrders.M_Merchants = merch;
                    }

                    ctx.SaveChanges();
                    transaction.Commit();
                }
            }

            SsData.EmptyCaches();
            return ApiResponse.Success;
        }

        /// <summary>
        ///     Приминяет настройки магазина ко всем дочерним уровням
        /// </summary>
        /// <param name="merchantId">Идентификатор магазина</param>
        /// <returns>Ответ API</returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse ApplySettingsAffiliate(Guid merchantId)
        {
            using (var ctx = SsData.GetContext())
            {
                var merch = ctx.M_Merchants.Single(x => x.Merchant_ID == merchantId);
                merch.DeepCopySettings(ctx);
                ctx.SaveChanges();
                return ApiResponse.Success;
            }
        }

        /// <summary>
        ///     Добавляет нового контрагента
        /// </summary>
        /// <param name="request">Запрос для добавления контрагента</param>
        /// <returns>Ответ API</returns>
        [AllowAnonymous]
        [HttpPost]
        public ApiResponse AddContractor([FromBody]AddContractorRequest request)
        {
            var ctx = SsData.GetContext();
            var newMerch = M_Merchants.NewMerchant;
            newMerch.Company = request.Company;
            newMerch.Name = request.ContractorName;
            newMerch.FullName = request.ContractorName;
            newMerch.Inn = "Not set";
            newMerch.Ogrn = "Not set";
            newMerch.AffiliateType = MerchantAffiliateType.Contractor;

            newMerch.InitializeFeeSettings();

            ctx.M_Merchants.Add(newMerch);
            ctx.SaveChanges();

            return ApiResponse.Success;
        }

        /// <summary>
        ///     Добавляет виртуальную структуру
        /// </summary>
        /// <param name="request">Запрос на добавление виртуальной структуры</param>
        /// <returns>Ответ API</returns>
        [AllowAnonymous]
        [HttpPost]
        public ApiResponse AddVirtualMerchant([FromBody]AddVirtualMerchantRequest request)
        {
            var ctx = SsData.GetContext();
            var parentId = new Guid(request.MerchantId);
            var parentMerch = ctx.M_Merchants.Single(x => x.Merchant_ID == parentId);
            var newMerch = M_Merchants.NewMerchant;

            newMerch.Company = request.Company;
            newMerch.Inn = parentMerch.Inn;
            newMerch.Ogrn = parentMerch.Ogrn;
            newMerch.Parent_ID = parentId;
            newMerch.Name = request.VirtualMerchantName;
            newMerch.FullName = request.VirtualMerchantName;
            newMerch.AffiliateType = MerchantAffiliateType.VirtualEntity;
            newMerch.CopySettingsAuto(parentMerch, ctx);

            newMerch.InitializeFeeSettings();

            ctx.M_Merchants.Add(newMerch);
            ctx.SaveChanges();

            return ApiResponse.Success;
        }

        /// <summary>
        ///     Присоединение к магазину дочерних юрлиц
        /// </summary>
        /// <param name="request">Запрос на присодениение дочерних юрлиц</param>
        /// <returns>Ответ API</returns>
        [AllowAnonymous]
        [HttpPost]
        public ApiResponse AttachLegals([FromBody]AttachLegalsRequest request)
        {
            var ctx = SsData.GetContext();
            var parentId = new Guid(request.MerchantId);
            var legalIds = request.LegalIds.Select(x => new Guid(x)).ToArray();
            var parentMerch = ctx.M_Merchants.Single(x => x.Merchant_ID == parentId);
            var legals = ctx.M_Merchants.Where(x => legalIds.Contains(x.Merchant_ID)).ToList();
            legals.ForEach(legal =>
            {
                legal.CopySettingsAuto(parentMerch, ctx);
                legal.Parent_ID = parentId;
            });

            ctx.sp_UpdateMerchantRootTable();
            ctx.SaveChanges();

            return ApiResponse.Success;
        }

        /// <summary>
        ///     Удаляет магазин из системы
        /// </summary>
        /// <param name="merchantId">Идентификатор магазина</param>
        /// <returns>Ответ API</returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse Delete(Guid merchantId)
        {
            using (var ctx = SsData.GetContext())
            {
                if (ctx.A_Applications.Any(x => x.Merchant_ID == merchantId))
                    return new ApiResponse("Merchant", "У данного магазина есть заявки");

                var merch = ctx.M_Merchants.Single(x => x.Merchant_ID == merchantId);
                if (merch.ChildMerchants.Any())
                    return new ApiResponse("Merchant", "У данного магазина есть дочерние филиалы");

                if (merch.M_MerchantFeeSettings != null)
                    ctx.M_MerchantFeeSettings.Remove(merch.M_MerchantFeeSettings);
                if (merch.M_MerchantDirectOrders != null)
                    ctx.M_MerchantDirectOrders.Remove(merch.M_MerchantDirectOrders);
                if (merch.M_MerchantDisabledCreditProducts.Any())
                    ctx.M_MerchantDisabledCreditProducts.RemoveRange(merch.M_MerchantDisabledCreditProducts);
                if (merch.M_Terminals.Any())
                    ctx.M_Terminals.RemoveRange(merch.M_Terminals);
                if (merch.M_GoodsCategoryFeeRateSettings.Any())
                    ctx.M_GoodsCategoryFeeRateSettings.RemoveRange(merch.M_GoodsCategoryFeeRateSettings);
                if (merch.M_MerchantFinanceOrgs.Any())
                    ctx.M_MerchantFinanceOrgs.RemoveRange(merch.M_MerchantFinanceOrgs);
                if (merch.M_FinOrgFeeRateSettings.Any())
                    ctx.M_FinOrgFeeRateSettings.RemoveRange(merch.M_FinOrgFeeRateSettings);
                if (merch.M_Goods.Any())
                    ctx.M_Goods.RemoveRange(merch.M_Goods);
                ctx.M_Merchants.Remove(merch);

                ctx.sp_UpdateMerchantRootTable();
                ctx.SaveChanges();
                return ApiResponse.Success;
            }
        }

        /// <summary>
        ///     Открепляет магазин от родительского уровня
        /// </summary>
        /// <param name="merchantId">Идентификатор магазина</param>
        /// <returns>Ответ API</returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse Detach(Guid merchantId)
        {
            using (var ctx = SsData.GetContext())
            {
                var merch = ctx.M_Merchants.Single(x => x.Merchant_ID == merchantId);
                if (merch.AffiliateType != MerchantAffiliateType.LegalEntity)
                    return new ApiResponse("Merchant", "Открепить можно только юрлицо");

                merch.Parent_ID = null;
                ctx.sp_UpdateMerchantRootTable();
                ctx.SaveChanges();
                return ApiResponse.Success;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ApiResponse SetPassword(MerchantPassword model)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var merch = context.M_Merchants.Single(m => m.Merchant_ID == model.MerchantId);

                merch.PasswordHash = Security.ComputePasswordHash(merch.PasswordSalt, model.Password);
                context.SaveChanges();
                SsData.EmptyCaches();
                return ApiResponse.Success;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ApiResponse ShipOrder(Guid applicationId)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var app = context.A_Applications.Single(a => a.Application_ID == applicationId);
                if (!app.HasLastStatus(ApplicationStatus.WaitingForGoodsShipment))
                    return new ApiResponse("ApplicationStatus", "Статус заявки не соответствует требуемому");

                
                app.ChangeStatus(ApplicationStatus.GoodsDeliveredToClient);
                context.SaveChanges();
                app.GetShipmentConfirmationStrategy()?.ConfirmShipment(app);
                return ApiResponse.Success;
            }
        }

        /// <summary>
        ///  Смена статуса на L2 (Документ подписан)
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse MarkDocumentAsSigned(Guid applicationId)
        {
             // Простановка статусов происходит внутри методов интеграций по активации кредита
             return new ApiJsonClient(SevenSeconds.Helpers.ConfigurationHelper.CoreApiUrl).Get<ApiResponse>("/api/finorg/activatecredit?applicationId=" + applicationId);
        }

        /// <summary>
        ///  Смена статуса на L3 (Документ передан в ФО)
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse MarkDocumentAsSendedToFinOrg(Guid applicationId)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var app = context.A_Applications.Single(a => a.Application_ID == applicationId);
                //app.EnsureStatus(ApplicationStatus.DocumentSigned);
                //app.AddNewStatus(ApplicationStatus.DocsDeliveredToFinOrg);
                context.SaveChanges();
                return ApiResponse.Success;
            }
        }


        /// <summary>
        ///  Отказ от подписания
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="refuseReasonId">ID причины отказа от подписания.</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ApiResponse MarkDocumentAsRefusalToSign(Guid applicationId, int refuseReasonId)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                if (!context.D_SignRefuseReason.Any(x => x.Id == refuseReasonId))
                    return new ApiResponse("ReasonRequired", "Не указана причина отказа");

                var app = context.A_Applications.Single(a => a.Application_ID == applicationId);
                var signDocsHLCourier = app.AcceptedOffer.F_FinanceOrganizations.SignOnPaper && !(app.AcceptedOffer.F_FinanceOrganizations.IsSignAutonomously ?? false);
                app.SignRefuseReasonId = refuseReasonId;
                
                //R14 Отказ от встречи с курьером
                if (signDocsHLCourier &&
                    (app.HasLastStatus(ApplicationStatus.DeliveryDateReceived) ||
                    app.HasLastStatus(ApplicationStatus.GoodsAreAvailable) ||
                    app.HasLastStatus(ApplicationStatus.PostponedSignDate)))
                {
                    app.ChangeStatus(ApplicationStatus.RejectingMeetingCourier);
                    context.SaveChanges();
                    return ApiResponse.Success;
                }
                
                //R15 со статуса ссуда открыта или верификация не пройдена
                if(signDocsHLCourier && 
                    (app.HasLastStatus(ApplicationStatus.LoanOpened) || 
                    app.HasLastStatus(ApplicationStatus.VerificationFailed) ||
                    app.HasLastStatus(ApplicationStatus.VerificationFO) ||
                    app.HasLastStatus(ApplicationStatus.VerificationFOFailed)))
                {
                    app.ChangeStatus(ApplicationStatus.DenAtSignCust);
                    context.SaveChanges();
                    return ApiResponse.Success;
                }
                
                //Со статусов Курьер отправлен, отправлено на верификацию, верификация пройдена - возможен переход на разные отказы, поэтому смотрим причину отказа
                if (app.HasLastStatus(ApplicationStatus.CourierSentToCustomer) || app.HasLastStatus(ApplicationStatus.SentForVerification) || app.HasLastStatus(ApplicationStatus.VerificationPassed))
                {
                    //R15 - отказ клиента
                    if (refuseReasonId == (int)SignRefuseReasons.NoProduct || refuseReasonId == (int)SignRefuseReasons.UnacceptableLoanConditions || refuseReasonId == (int)SignRefuseReasons.NoCreditNeeded)
                    {
                        app.ChangeStatus(ApplicationStatus.DenAtSignCust);
                        context.SaveChanges();
                        return ApiResponse.Success;
                    }
                    //R13 - отказ курьера
                    else
                    {
                        app.ChangeStatus(ApplicationStatus.DenialAtSign7S);
                        context.SaveChanges();
                        return ApiResponse.Success;
                    }
                }

                return new ApiResponse("BadAppStatus", "Действие невозможно для заявки в данном статусе"); ;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ApiResponse ClientDebtStatus(DebtStatusRequest request)
        {
            using (var context = new SevenSecondsCoreEntities())
            {
                var app = context.A_Applications.Single(a => a.Application_ID == request.ApplicationID);

                if (app.AcceptedOffer == null)
                {
                    return new ApiResponse("offer", "У анкеты нет выбранного кредитного предложения");
                }

                DebtStatus debtState;
                using(var integrationClient = app.AcceptedOffer.F_FinanceOrganizations.GetIntegrationClient())
                    debtState = integrationClient.DebtStatusRequest(app.Application_ID);

                if (debtState == null)
                {
                    return new ApiResponse("financeOrganization", "Финансовая организация не поддерживает запрос задолженности");
                }

                var result = new DebtStatusResponse
                {
                    result = true,
                    ApplicationId = app.Application_ID,
                    DebtStatus = (debtState.TodayPayment > 0) ? 1 : 2,
                    BasicDebt = (int)(debtState.TodayPayment * 100),
                    Intersect = 0, //Финорги-молчуны. Не знаем.
                    StatusDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    FinOrg = app.AcceptedOffer.F_FinanceOrganizations.Name,
                    AgreementNumber = app.AcceptedOffer.FinanceOrgContract
                };

                return result;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ApiResponse RequestCategories(Guid applicationId)
        {
            GoodsHelper.RequestCategories(applicationId);
            return ApiResponse.Success;
        }

        private Dictionary<string, string> ProcessMerchantCreditProducts(ICollection<M_MerchantDisabledCreditProducts> currentDisabledProducts, ICollection<Guid> disabledProducts, Guid merchantId, SevenSecondsCoreEntities ctx)
        {
            Dictionary<string, string> result = null;
            if (disabledProducts != null)
            {
                //checked products
                var uncheckedProducts = new List<M_MerchantDisabledCreditProducts>();
                foreach (var product in currentDisabledProducts)
                {
                    if (!disabledProducts.Contains(product.Product_ID))
                    {
                        uncheckedProducts.Add(product);
                    }
                }

                foreach (var product in uncheckedProducts)
                {
                    currentDisabledProducts.Remove(product);
                    ctx.Entry(product).State = System.Data.Entity.EntityState.Deleted;
                }

                //unchecked products
                foreach (var productId in disabledProducts)
                {
                    if (!currentDisabledProducts.Any(x => x.Product_ID == productId))
                    {
                        currentDisabledProducts.Add(new M_MerchantDisabledCreditProducts
                        {
                            ID = Guid.NewGuid(),
                            Merchant_ID = merchantId,
                            Product_ID = productId
                        });
                    }
                }
            }

            return result;
        }

        private void ProcessMerchantStatusCallbackSettings(M_Merchants merch, ICollection<CallbackSettings> callbackSettings, SevenSecondsCoreEntities ctx)
        {
            if (callbackSettings == null)
                return;

            var existingSettings = merch.M_StatusCallbackSettings.ToList();
            callbackSettings.ToList().ForEach(setting =>
            {
                var existing = existingSettings.SingleOrDefault(x => x.StatusType_ID == setting.Id);
                if (existing != null)
                {
                    if (setting.Enabled)
                    {
                        ctx.M_StatusCallbackSettings.Remove(existing);
                    }
                    else
                    {
                        existing.Disabled = true;
                    }
                }
                else
                { 
                    var existingPrescore = existingSettings.SingleOrDefault(x => x.PrescoreStatusType_ID == setting.Id);
                    if (existingPrescore != null)
                    {
                        if (setting.Enabled)
                        {
                            ctx.M_StatusCallbackSettings.Remove(existingPrescore);
                        }
                        else
                        {
                            existingPrescore.Disabled = true;
                        }
                    }
                    else
                    {
                        if (!setting.Enabled)
                        {
                            ctx.M_StatusCallbackSettings.Add(new M_StatusCallbackSettings(merch, setting));
                        }
                    }
                }
            });
        }
    }
}