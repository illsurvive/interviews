﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SevenSeconds.Integration.Finance.TransCapitalBank.Models.Responses
{
    public class SimpleIdentificationResponse
    {
        public string ExtId { get; set; }
        public long OrderId { get; set; }
        public IdentificationResult Inn { get; set; }
        public IdentificationResult Snils { get; set; }
        public IdentificationResult Passport { get; set; }
        public IdentificationResult PassportDeferred { get; set; }

        public string AdapterName { get; set; }
        public string Code { get; set; }
        public string ExceptionType { get; set; }
        public string Message { get; set; }
        public Properties Properties { get; set; }
        public string SessionId { get; set; }
        public string SourcePath { get; set; }
        public string SourceType { get; set; }
        public string TimeStamp { get; set; }
    }

    public class IdentificationResult
    {
        public string Status { get; set; }
        public string Description { get; set; }
    }

    public class Properties
    {
        public string CodeNumber { get; set; }
        public string CodeType { get; set; }
        public string OperationId { get; set; }
    }
}
